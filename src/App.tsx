import React from 'react';
import './App.css';
import {ECharts} from './components/Echarts/Echarts'
import Modal from 'react-modal';

Modal.setAppElement('#root');

function App() {
  return (
    <div className="App">
      <ECharts />
    </div>
  );
}

export default App;
