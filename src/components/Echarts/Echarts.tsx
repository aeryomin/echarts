import React, { useState } from 'react'
// import { useInitChartData } from './hooks/useInitChartData';
import Modal from 'react-modal'

import ReactEcharts from 'echarts-for-react'
// import { gradient } from './utils/colors'
import { toHeatmapDto } from './utils/toHeatmapDto'
import { getWeightsNormalized } from './utils/getWeightsNormalized'
import { getOptions } from './utils/getOptions'

interface IProps {}

enum EChartType {
  Qualitative = 'Qualitative',
  Quantitative = 'Quantitative',
}

const qualitativeProbability = [
  // 'NotAvailable',
  'unlikely',
  'possible',
  'likely',
  'probable',
  'highly probable',
] as const

// type QualitativeProbability = typeof qualitativeProbability[number];

const qualitativeImpact = ['Major', 'Significant', 'Moderate', 'Minor', 'Marginal'] as const

const qualitativeProbabilityWeights = 1 / (qualitativeProbability.length - 1)
const qualitativeImpactWeights = 1 / (qualitativeImpact.length - 1)

const chartDimensions = {
  width: 700,
  height: 600,
}

const COLORS_RANGE = ['#04bf1d', '#f4fc05', '#ff0008']

const qualitativeToQuantitativeDto = (data: unknown[][]) => {
  return data.map(it => {
    const probabilityWeight =
      qualitativeProbability.findIndex(probability => probability === it[0]) * qualitativeProbabilityWeights
    const impactWeight = qualitativeImpact.findIndex(impact => impact === it[1]) * qualitativeImpactWeights
    return probabilityWeight * impactWeight
  })
}

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '500px',
  },
}

type ChartMode = 'scatter' | 'heatmap' | null

export const ECharts: React.FC<IProps> = () => {
  // const sourceDataQuantitative = useInitChartData();

  const [chartType, setChartType] = useState<EChartType>(EChartType.Quantitative)
  const [selectedRisk, setSelectedRisk] = useState<null | number>(null)
  const [chartMode, setChartMode] = useState<ChartMode>(null)

  // const getData = (initialSource: ReturnType<typeof useInitChartData>) => {
  //     return initialSource.map(record => {
  //         return [record.quantitativeProbability, record.quantitativeImpact];
  //     });
  // };

  const [modalIsOpen, setIsOpen] = useState(false)

  function openModal() {
    setIsOpen(true)
  }

  function closeModal() {
    setIsOpen(false)
  }

  const onClick = (params: any) => {
    setSelectedRisk(params.value[2] as number)
    setChartMode(params.seriesType)
    openModal()
  }

  const onEvents = {
    click: onClick,
  }

  const quantitativeData = [
    [0, 0, 5], // x, y, label
    [0.99, 99000, 7],
    [0.9, 80000, 3],
    [0.1, 10000, 8],
    [0.2, 40000, 1],
    [0.74, 55000, 10],
    [0.35, 43000, 2],
    [0.6, 60000, 6],
    [0.5, 80000, 2],
    [0.23, 90000, 9],
    [0.5, 50000, 4],
    [0.7, 70000, 3],
    [0.8, 80000, 5],
    [0.9, 90000, 6],
    [0.3, 30000, 2],
    [0.2, 20000, 9],
    [0.7, 20000, 7],
    [0, 99000, 10],
    [0.99, 0, 3],
    [0.9, 50000, 8],
    [0.9, 30000, 3],
    [0.6, 100000, 8],
    [0.3, 70000, 9],
  ]
  const qualitativeData: any = [
    ['unlikely', 'Major', 3],
    ['likely', 'Significant', 5],
    ['probable', 'Major', 8],
    ['probable', 'Moderate', 6],
    ['highly probable', 'Minor', 1],
    ['probable', 'Marginal', 10],
    ['highly probable', 'Marginal', 3],
    ['probable', 'Minor', 5],
    ['possible', 'Moderate', 7],
    ['likely', 'Marginal', 5],
  ]

  const qualitativeWeights = qualitativeToQuantitativeDto(qualitativeData)
  const quantitativeWeights = getWeightsNormalized(quantitativeData)

  const sourceDataQuantitative: unknown[][] = quantitativeData.map((data, index) => [
    ...data,
    quantitativeWeights[index],
  ])

  const sourceDataQualitative: unknown[][] = qualitativeData.map((data: unknown[][], index: number) => [
    ...data,
    qualitativeWeights[index],
  ])

  const optionsBubbleQuantitative = getOptions({
    type: 'scatter',
    source: sourceDataQuantitative,
    colorRange: COLORS_RANGE,
    xAxisType: 'value',
    yAxisType: 'value',
    zoom: true,
    chartName: 'Bubble-Quantitative'
  })
  const optionsBubbleQualitative = getOptions({
    type: 'scatter',
    source: sourceDataQualitative,
    colorRange: COLORS_RANGE,
    xAxisData: qualitativeProbability,
    yAxisData: qualitativeImpact,
    xAxisType: 'category',
    yAxisType: 'category',
    zoom: true,
    chartName: 'Bubble-Qualitative'
  })
  const optionsHeatmapQuantitative = getOptions({
    type: 'heatmap',
    source: toHeatmapDto(sourceDataQuantitative as number[][]),
    colorRange: COLORS_RANGE,
    xAxisData: Array(11)
      .fill(0)
      .map((_, index) => (index * 0.1).toFixed(1)),
    yAxisData: Array(11)
      .fill(0)
      .map((it, index) => (it + index * 10000).toString()),
    xAxisType: 'category',
    yAxisType: 'category',
    chartName: 'Heatmap-Quantitative'
  })
  const optionsHeatmapQualitative = getOptions({
    type: 'heatmap',
    source: sourceDataQualitative,
    colorRange: COLORS_RANGE,
    xAxisData: qualitativeProbability,
    yAxisData: qualitativeImpact,
    xAxisType: 'category',
    yAxisType: 'category',
    chartName: 'Heatmap-Qualitative'
  })

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'start' }}>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'start' }}>
          <label>
            <input
              type="radio"
              value={chartType}
              name={EChartType.Quantitative}
              checked={chartType === EChartType.Quantitative}
              onChange={() => {
                setChartType(prev =>
                  prev === EChartType.Quantitative ? EChartType.Qualitative : EChartType.Quantitative,
                )
              }}
            />
            {''} {EChartType.Quantitative}
          </label>
          <br />
          <label>
            <input
              type="radio"
              value={chartType}
              name={EChartType.Qualitative}
              checked={chartType === EChartType.Qualitative}
              onChange={() => {
                setChartType(prev =>
                  prev === EChartType.Quantitative ? EChartType.Qualitative : EChartType.Quantitative,
                )
              }}
              disabled={false}
            />
            {''} {EChartType.Qualitative}
          </label>
        </div>
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '24px' }}>
        <b>{chartType}</b>
        {'\u00A0mode'}
      </div>
      <div>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <ReactEcharts
            option={chartType === EChartType.Quantitative ? optionsBubbleQuantitative : optionsBubbleQualitative}
            notMerge={false}
            lazyUpdate={false}
            style={{ width: `${chartDimensions.width}px`, height: `${chartDimensions.height}px` }}
            // onChartReady={this.onChartReadyCallback}
            onEvents={onEvents}
            // opts={}
          />
          <ReactEcharts
            option={chartType === EChartType.Quantitative ? optionsHeatmapQuantitative : optionsHeatmapQualitative}
            notMerge={true}
            lazyUpdate={false}
            style={{ width: `${chartDimensions.width}px`, height: `${chartDimensions.height}px` }}
            onEvents={onEvents}
          />
        </div>
      </div>
      <br />
      <div>
        {/*Selected risk: <b>{selectedRisk}</b>*/}
      </div>
      <br />
      <Modal isOpen={modalIsOpen} onRequestClose={closeModal} style={customStyles} contentLabel="Example Modal">
        <h2>Action modal</h2>
        <div>
          Selected <b>{selectedRisk}</b> risks for{' '}
          {chartMode === 'scatter' ? <b>'Bubble Chart'</b> : <b>'Heatmap Chart'</b>}
        </div>
        <br />
        <button onClick={closeModal}>close</button>
      </Modal>
    </div>
  )
}
