function rgb(color: string) {
    const hex = color.replace("#", "")
    return {
        r: parseInt(hex.substring(0, 2), 16),
        g: parseInt(hex.substring(2, 4), 16),
        b: parseInt(hex.substring(4, 6), 16),
    }
}

function hex(num: number) {
    const res = num.toString(16)
    return (res.length === 1) ? '0' + res : res;
}

export function gradient(color1: string, color2: string, ratio: number): string {
    const from = rgb(color1)
    const to = rgb(color2)

    const r = Math.ceil(from.r * (1 - ratio) + to.r * (ratio));
    const g = Math.ceil(from.g * (1 - ratio) + to.g * (ratio));
    const b = Math.ceil(from.b * (1 - ratio) + to.b * (ratio));

    return "#" + hex(r) + hex(g) + hex(b);
}