import { formatDate } from './formatDate'

interface IGetOptions {
  type: string
  source: unknown[][]
  colorRange: string[]
  xAxisData?: any
  yAxisData?: any
  xAxisType: string
  yAxisType: string
  zoom?: boolean
  chartName: string
}

export const getOptions = ({
  type,
  source,
  colorRange,
  xAxisData,
  yAxisData,
  xAxisType,
  yAxisType,
  zoom = false,
  chartName,
}: IGetOptions) => {
  const date = formatDate(new Date())
  return {
    legend: {},
    animation: false,
    dataset: {
      source: source,
    },
    toolbox: {
      show: true,
      feature: {
        saveAsImage: {
          type: 'jpg',
          name: `${chartName}_${date}`,
        },
      },
    },
    series: [
      {
        type: type,
        label: {
          show: true,
          formatter: '{@[2]}',
        },
        tooltip: {},
      },
    ],
    xAxis: {
      type: xAxisType,
      data: xAxisData,
      name: 'Probability',
      nameLocation: 'middle',
      nameGap: 40,
      splitLine: {
        show: true,
      },
      axisLabel: {
        margin: 18,
        fontSize: 12,
        interval: 0,
      },
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: 16,
      },
    },
    yAxis: {
      type: yAxisType,
      data: yAxisData,
      name: 'Impact',
      nameLocation: 'middle',
      nameGap: 100,
      splitLine: {
        show: true,
      },
      axisLabel: {
        margin: 32,
        fontSize: 12,
        interval: 0,
        formatter: (value: unknown) => {
          return value
        },
      },
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: 16,
      },
    },
    visualMap: [
      {
        show: false,
        dimension: 2, // means the 3rd column
        min: 1, // lower bound
        max: 10, // higher bound
        inRange: {
          symbolSize: [15, 60],
        },
      },
      {
        show: false,
        dimension: 3,
        min: 0, // lower bound
        max: 1,
        inRange: {
          color: colorRange,
          opacity: Array(source.length).fill(1),
        },
      },
    ],
    grid: {
      left: '10%',
      right: '7%',
      bottom: '5%',
      containLabel: true,
    },
    dataZoom: zoom && [
      {
        type: 'inside',
        xAxisIndex: [0],
      },
      {
        type: 'slider',
        xAxisIndex: [0],
      },
      {
        type: 'inside',
        yAxisIndex: [0],
      },
      {
        type: 'slider',
        yAxisIndex: [0],
      },
    ],
  }
}
