export const toHeatmapDto = (data: number[][]) => {
  return data.map(it => [Math.round(it[0] * 10), it[1] / 10000, it[2], it[3]])
}