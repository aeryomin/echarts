// function randomNumber(min: number, max: number) {
//   return Math.round(Math.random() * (max - min) + min)
// }

function getLimits(data: number[]): { min: number; max: number } {
  let min = data[0]
  let max = 0
  data.forEach(it => {
    if (it < min) {
      min = it
    }
    if (it > max) {
      max = it
    }
  })
  return { min, max }
}

const calcWeight = (value: number, valueLimits: { min: number; max: number }): number => {
  return (value - valueLimits.min) / (valueLimits.max - valueLimits.min)
}

export const getWeightsNormalized = (data: number[][]): number[] => {
  const probabilities = data.map(it => it[0])
  const impacts = data.map(it => it[1])
  const probabilityLimits = getLimits(probabilities)
  const impactLimits = getLimits(impacts)

  const weights = data.map(it => {
    const probabilityWeight = calcWeight(it[0], probabilityLimits)
    const impactWeight = calcWeight(it[1], impactLimits)
    return probabilityWeight * impactWeight
  })
  const weightsLimits = getLimits(weights)
  return weights.map(it => calcWeight(it, weightsLimits))
}