import { useMemo } from 'react';

// import riskRegisterData from './riskRegister.json'
import riskEntriesData from '../mocks/riskEntries.json';
import riskResponsesData from '../mocks/riskResponses.json';
import riskEvaluationsData from '../mocks/riskEvaluations.json';

// const riskEntriesIds = riskRegisterData.result.results[0][0][0].risk_entries

const riskEntries = riskEntriesData.result.results.flat(2);

const riskResponsesIdsFromRiskEntries = riskEntries.flatMap(riskEntry => riskEntry.risk_responses);
// console.log(riskResponsesIdsFromRiskEntries);

const allRiskResponses = riskResponsesData.result.results.flat(2);
const allRiskEvaluations = riskEvaluationsData.result.results.flat(2);
// // console.log({riskEntries})

const fullRiskResponsesFromRiskEntries = allRiskResponses.filter(response =>
    riskResponsesIdsFromRiskEntries.includes(response.instance_id),
);
// console.log('riskResponsesFromFromRiskEntries', fullRiskResponsesFromRiskEntries);

const riskQualitativeEvaluationsIds = fullRiskResponsesFromRiskEntries
    .map(response => response.residual_qualitative_evaluation)
    .filter(it => it);
const riskQuantitativeEvaluationsIds = fullRiskResponsesFromRiskEntries
    .map(response => response.residual_quantitative_evaluation)
    .filter(it => it);
// console.log('riskQualitativeEvaluationsIds', riskQualitativeEvaluationsIds);
// console.log('riskQuantitativeEvaluationsIds', riskQuantitativeEvaluationsIds);

const fullRiskQualitativeEvaluations = allRiskEvaluations.filter(evaluation =>
    riskQualitativeEvaluationsIds.includes(evaluation.instance_id),
);
const fullRiskQuantitativeEvaluations = allRiskEvaluations.filter(evaluation =>
    riskQuantitativeEvaluationsIds.includes(evaluation.instance_id),
);

export const useInitChartData = () => {
    return useMemo(() => {
        return riskResponsesIdsFromRiskEntries.map(responseId => {
            const riskResponse = allRiskResponses.find(response => response.instance_id === responseId);
            const riskResponseQualitativeEvaluationId = riskResponse?.residual_qualitative_evaluation;
            const riskResponseQuantitativeEvaluationId = riskResponse?.residual_quantitative_evaluation;

            return {
                responseId,
                qualitativeImpact: fullRiskQualitativeEvaluations.find(
                    evaluation => evaluation.instance_id === riskResponseQualitativeEvaluationId,
                )?.impact,
                qualitativeImpactProbability: fullRiskQualitativeEvaluations.find(
                    evaluation => evaluation.instance_id === riskResponseQualitativeEvaluationId,
                )?.probability,
                quantitativeImpact: fullRiskQuantitativeEvaluations.find(
                    evaluation => evaluation.instance_id === riskResponseQuantitativeEvaluationId,
                )?.impact,
                quantitativeProbability: fullRiskQuantitativeEvaluations.find(
                    evaluation => evaluation.instance_id === riskResponseQuantitativeEvaluationId,
                )?.probability,
                riskEntryId: riskResponsesIdsFromRiskEntries.find(riskEntryId => riskEntryId === responseId),
            };
        });
    }, []);
};
